'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _assign = require('babel-runtime/core-js/object/assign');

var _assign2 = _interopRequireDefault(_assign);

var _core = require('../core');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * (主动)消息发送控制器
 * @type {Array}
 */
exports.default = {
  /**
   * 向用户发送投稿确认信息
   * @param  {Object} message Message
   * @return {[type]}         [description]
   */
  subAsk: function subAsk(message) {
    var inline_keyboard = [[{ text: _core.lang.get('yes'), callback_data: _core.vars.SUB_REAL }, { text: _core.lang.get('no'), callback_data: _core.vars.SUB_ANY }]];
    var reply_to_message_id = message.message_id ? message.message_id : message.media[0].message_id;
    var text = _core.lang.get('sub_confirm_tip');
    
    inline_keyboard.push([{ text: _core.lang.get('sub_button_cancel'), callback_data: _core.vars.SUB_CANCEL }]);
    _core.bot.sendMessage(message.chat.id, text, {
      reply_to_message_id: reply_to_message_id,
      reply_markup: { inline_keyboard: inline_keyboard },
      parse_mode: 'MarkdownV2'
    });
  },

  /**
   * 编辑一条信息
   * @param  {[type]} text    [description]
   * @param  {[type]} params  {message_id, chat_id}
   * @return {[type]}         [description]
   */
  editMessage: function editMessage(text) {
    var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    var _params = (0, _assign2.default)({
      parse_mode: 'MarkdownV2'
    }, params);
    return _core.bot.editMessageText(text, _params);
  },

  /**
   * 编辑当前的消息
   * @param  {[type]} text    [description]
   * @param  {[type]} message [description]
   * @param  {Object} params  [description]
   * @return {[type]}         [description]
   */
  editCurrentMessage: function editCurrentMessage(text, message) {
    var params = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

    var _params = (0, _assign2.default)({
      chat_id: message.chat.id,
      message_id: message.message_id
    }, params);
    return this.editMessage(text, _params);
  },

  /**
   * 使用现有结构发送消息
   * @param  {[type]} text    [description]
   * @param  {[type]} message [description]
   * @param  {Object} params  [description]
   * @return {[type]}         [description]
   */
  sendCurrentMessage: function sendCurrentMessage(text, message) {
    var params = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

    return this.sendMessage(message.chat.id, text, params);
  },

  /**
   * 发送消息,默认使用markdown
   * @param  {[type]} chatId [description]
   * @param  {[type]} text   [description]
   * @param  {[type]} params [description]
   * @return {[type]}        [description]
   */
  sendMessage: function sendMessage(chatId, text, params) {
    var _params = (0, _assign2.default)({
      parse_mode: 'MarkdownV2'
    }, params);
    return _core.bot.sendMessage(chatId, text, _params);
  },

  /**
   * 将消息转发到审稿群
   * @param  {Object}  reply_to_message Message
   * @param  {String}  type    投稿类型
   * @return {Promise}         成功后返回转发后的Message对象
   * {reply_to_message_id: 审稿群actionMsg应该回复的稿件消息ID，message，稿件}
   */
  forwardMessage: function forwardMessage(reply_to_message, type) {
    var _this = this;

    return (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
      var condition, message, fwdMsg, respMsg;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              condition = _core.subs.getMsgCondition(reply_to_message);
              message = _core.subs.one(condition);
              fwdMsg = {}, respMsg = {};
              // 若是mediagroup消息

              if (!message.media_group_id) {
                _context.next = 10;
                break;
              }

              _context.next = 6;
              return _core.bot.sendMediaGroup(_core.config.Group, message.media);

            case 6:
              fwdMsg = _context.sent[0];

              // 将审稿群的mediagroupId写到mediaGroup消息的fwdMsgGroupId节点
              respMsg = _core.subs.update(condition, { fwdMsgGroupId: fwdMsg.media_group_id, sub_type: type });
              _context.next = 14;
              break;

            case 10:
              _context.next = 12;
              return _core.bot.forwardMessage(_core.config.Group, message.chat.id, message.message_id);

            case 12:
              fwdMsg = _context.sent;
              // 转发至审稿群
              respMsg = _core.subs.update(condition, { fwdMsgId: fwdMsg.message_id, sub_type: type });

            case 14:
              return _context.abrupt('return', { reply_to_message_id: fwdMsg.message_id, message: respMsg });

            case 15:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, _this);
    }))();
  },

  /**
   * 询问管理员如何处理稿件
   * @param  {Object} {reply_to_message_id: 审稿群actionMsg应该回复的稿件消息ID，message，稿件}
   * @return {[type]}         [description]
   */
  askAdmin: function askAdmin(_ref) {
    var _this2 = this;

    var reply_to_message_id = _ref.reply_to_message_id,
      message = _ref.message;
    return (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2() {
      var condition, text, from, actionMsg;
      return _regenerator2.default.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              condition = _core.subs.getMsgCondition(message);
              text = _core.lang.getAdminAction(message);
              from = message.sub_type == _core.vars.SUB_ANY ? 'anonymous' : 'real';
              _context2.next = 5;
              return _core.bot.sendMessage(_core.config.Group, text, {
                reply_to_message_id: reply_to_message_id,
                parse_mode: 'MarkdownV2',
                disable_web_page_preview: true,
                reply_markup: {
                  resize_keyboard: true,
                  inline_keyboard: [[{ text: _core.lang.get('button_receive'), callback_data: 'receive:' + from }]]
                }
              });

            case 5:
              actionMsg = _context2.sent;

              _core.subs.update(condition, { actionMsgId: actionMsg.message_id }); // 更新actionMsgId

            case 7:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, _this2);
    }))();
  },


  /**
   * 推送频道消息
   * @param  {[type]} message [description]
   * @param  {[type]} params  comment=评论, isMute=是否静音
   * @return {[type]}         [description]
   */
  sendChannel: function sendChannel(message, params) {
    var _this3 = this;

    return (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3() {
      var resp, caption, options;
      return _regenerator2.default.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              resp = null;
              caption = _core.subs.getCaption(message, params);
              options = _core.subs.getOptions(message, caption, params);

              if (!message.media_group_id) {
                _context3.next = 10;
                break;
              }

              message.media[0].caption = caption;
              _context3.next = 7;
              return _core.bot.sendMediaGroup(_core.config.Channel, message.media);

            case 7:
              resp = _context3.sent;
              _context3.next = 43;
              break;

            case 10:
              if (!message.audio) {
                _context3.next = 16;
                break;
              }

              _context3.next = 13;
              return _core.bot.sendAudio(_core.config.Channel, message.audio.file_id, options);

            case 13:
              resp = _context3.sent;
              _context3.next = 43;
              break;

            case 16:
              if (!message.document) {
                _context3.next = 22;
                break;
              }

              _context3.next = 19;
              return _core.bot.sendDocument(_core.config.Channel, message.document.file_id, options);

            case 19:
              resp = _context3.sent;
              _context3.next = 43;
              break;

            case 22:
              if (!message.voice) {
                _context3.next = 28;
                break;
              }

              _context3.next = 25;
              return _core.bot.sendVoice(_core.config.Channel, message.voice.file_id, options);

            case 25:
              resp = _context3.sent;
              _context3.next = 43;
              break;

            case 28:
              if (!message.video) {
                _context3.next = 34;
                break;
              }

              _context3.next = 31;
              return _core.bot.sendVideo(_core.config.Channel, message.video.file_id, options);

            case 31:
              resp = _context3.sent;
              _context3.next = 43;
              break;

            case 34:
              if (!message.photo) {
                _context3.next = 40;
                break;
              }

              _context3.next = 37;
              return _core.bot.sendPhoto(_core.config.Channel, message.photo[0].file_id, options);

            case 37:
              resp = _context3.sent;
              _context3.next = 43;
              break;

            case 40:
              _context3.next = 42;
              return _core.bot.sendMessage(_core.config.Channel, caption, options);

            case 42:
              resp = _context3.sent;

            case 43:
              return _context3.abrupt('return', resp);

            case 44:
            case 'end':
              return _context3.stop();
          }
        }
      }, _callee3, _this3);
    }))();
  },

  /**
   * 审核稿件
   * @param  {Object} message             稿件，查询出来的
   * @param  {Object} receive             审稿人对象，一般是message.from
   * @param  {String} comment             附加评论
   * @param  {Boolean} isMute             是否静音推送
   */
  receiveMessage: function receiveMessage(message, receive) {
    var _this4 = this;

    var params = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
    return (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4() {
      var resp, condition, text, reply_to_message_id;
      return _regenerator2.default.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              if (!message.receive_date) {
                _context4.next = 2;
                break;
              }

              return _context4.abrupt('return', _core.bot.sendMessage(_core.config.Group, _core.lang.get('err_repeat'), {
                reply_to_message_id: message.fwdMsgId
              }));

            case 2:
              if (_core.helper.isMute()) {
                params.isMute = true;
              }
              _context4.next = 5;
              return _this4.sendChannel(message, params);

            case 5:
              resp = _context4.sent;
              condition = _core.subs.getMsgCondition(message);
              // 记录审稿人和时间

              message = _core.subs.update(condition, { receive: receive, receive_date: _core.helper.getTimestamp(), receive_params: params });
              // 获取审稿群通过审核文案
              text = _core.lang.getAdminActionFinish(message);
              // 编辑审稿群actionMsg

              _context4.next = 11;
              return _this4.editMessage(text, { chat_id: _core.config.Group, message_id: message.actionMsgId, disable_web_page_preview: true });

            case 11:
              reply_to_message_id = _core.subs.getReplytoMessageId(message);
              // 向用户发送稿件过审信息

              _context4.next = 14;
              return _core.bot.sendMessage(message.chat.id, _core.lang.get('sub_finish_tip'), { reply_to_message_id: reply_to_message_id });

            case 14:
              return _context4.abrupt('return', resp);

            case 15:
            case 'end':
              return _context4.stop();
          }
        }
      }, _callee4, _this4);
    }))();
  },

  /**
   * 拒绝投稿
   * @param  {[type]} message [description]
   * @param  {Object} reject  是谁操作的
   * @param  {String} reason  理由
   * @return {[type]}         [description]
   */
  rejectMessage: function rejectMessage(message, reject, reason) {
    var _this5 = this;

    return (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5() {
      var condition, rejectText, text, reply_to_message_id;
      return _regenerator2.default.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              if (!message.reject_date) {
                _context5.next = 2;
                break;
              }

              return _context5.abrupt('return', _core.bot.sendMessage(_core.config.Group, _core.lang.get('err_repeat_reject'), {
                reply_to_message_id: message.fwdMsgId
              }));

            case 2:
              condition = _core.subs.getMsgCondition(message);
              // 记录操作人和拒绝理由及时间

              message = _core.subs.update(condition, { reject: reject, reject_date: _core.helper.getTimestamp(), reject_reason: reason });
              rejectText = _core.lang.get('reject_tips', { reason: reason });
              // 获取审稿群拒绝审核文案

              text = _core.lang.getAdminActionReject(message, reason);
              // 编辑审稿群actionMsg

              reply_to_message_id = _core.subs.getReplytoMessageId(message);
              _context5.next = 9;
              return _this5.editMessage(text, { chat_id: _core.config.Group, message_id: message.actionMsgId, disable_web_page_preview: true });

            case 9:
              _context5.next = 11;
              return _core.bot.sendMessage(message.chat.id, rejectText, { reply_to_message_id: reply_to_message_id, parse_mode: 'MarkdownV2' });

            case 11:
              return _context5.abrupt('return', message);

            case 12:
            case 'end':
              return _context5.stop();
          }
        }
      }, _callee5, _this5);
    }))();
  },

  /**
   * 回复用户消息，同时用户将进入聊天状态
   * 用户可透过KeyboardButton退出聊天，管理员可透过/endre 结束会话
   * @param  {[type]} message 稿件
   * @param  {String} comment  管理员回复给用户的消息
   * @return {[type]}         [description]
   */
  replyMessage: function replyMessage(message, comment) {
    var _this6 = this;

    var reMode = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
    return (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee6() {
      return _regenerator2.default.wrap(function _callee6$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
              if (!reMode) {
                _context6.next = 3;
                break;
              }

              _context6.next = 3;
              return _core.re.start(message);

            case 3:
              _context6.next = 5;
              return _this6.sendCurrentMessage(_core.lang.get('re_comment', { comment: comment }), message);

            case 5:
              return _context6.abrupt('return', true);

            case 6:
            case 'end':
              return _context6.stop();
          }
        }
      }, _callee6, _this6);
    }))();
  },

  /**
   * 回复用户信息
   * @param  {[type]} options.msg    [description]
   * @param  {[type]} options.match  [description]
   * @param  {[type]} options.rep    [description]
   * @param  {[type]} options.repMsg [description]
   * @param  {String} command        /re 或者 /echo 
   * re 会进入会话状态， echo 只是发送，不进入会话
   * @return {[type]}                [description]
   */
  replyMessageWithCommand: function replyMessageWithCommand(_ref2) {
    var _this7 = this;

    var msg = _ref2.msg,
      match = _ref2.match,
      rep = _ref2.rep,
      repMsg = _ref2.repMsg;
    var command = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '/re';
    return (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee7() {
      var comment, message, chatMode, respMsg;
      return _regenerator2.default.wrap(function _callee7$(_context7) {
        while (1) {
          switch (_context7.prev = _context7.next) {
            case 0:
              if (!_core.helper.isPrivate(msg)) {
                _context7.next = 2;
                break;
              }

              return _context7.abrupt('return', false);

            case 2:
              comment = match[1];

              if (comment) {
                _context7.next = 5;
                break;
              }

              throw { message: _core.lang.get('admin_reply_err', { command: command }) };

            case 5:
              // 没有输入消息
              message = _core.subs.getMsgWithReply(repMsg);

              if (!(!message && !repMsg.forward_from)) {
                _context7.next = 8;
                break;
              }

              return _context7.abrupt('return', false);

            case 8:
              // 无从回复
              if (!message) {
                message = { chat: repMsg.forward_from, from: repMsg.forward_from };
              }
              chatMode = command == '/re' ? true : false;
              _context7.next = 12;
              return _this7.replyMessage(message, comment, chatMode);

            case 12:
              _context7.next = 14;
              return rep(_core.lang.get('re_send_success'));

            case 14:
              respMsg = _context7.sent;
              _context7.next = 17;
              return _core.helper.sleep(1000);

            case 17:
              _this7.editCurrentMessage("...", respMsg);
              _context7.next = 20;
              return _core.helper.sleep(2000);

            case 20:
              _core.bot.deleteMessage(respMsg.chat.id, respMsg.message_id);

            case 21:
            case 'end':
              return _context7.stop();
          }
        }
      }, _callee7, _this7);
    }))();
  },

  /**
   * 管理员点击采纳稿件(从actionMsg点击按钮)
   * @param  {Object}  query callback data
   * @return {Promise}       [description]
   */
  receive: function receive(query) {
    var _this8 = this;

    return (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee8() {
      var fwdMsg, condition, message;
      return _regenerator2.default.wrap(function _callee8$(_context8) {
        while (1) {
          switch (_context8.prev = _context8.next) {
            case 0:
              fwdMsg = query.message.reply_to_message; // 审稿群的稿件

              condition = _core.subs.getFwdMsgCondition(fwdMsg); // 得到查询条件

              message = _core.subs.one(condition); // 得到真实稿件

              _this8.receiveMessage(message, query.from);

            case 4:
            case 'end':
              return _context8.stop();
          }
        }
      }, _callee8, _this8);
    }))();
  }
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9oYW5kbGVyL21zZ0NvbnRyb2wuanMiXSwibmFtZXMiOlsic3ViQXNrIiwibWVzc2FnZSIsInllc2xhYmVsIiwiZm9yd2FyZF9kYXRlIiwibGFuZyIsImdldCIsImlubGluZV9rZXlib2FyZCIsInRleHQiLCJjYWxsYmFja19kYXRhIiwidmFycyIsIlNVQl9SRUFMIiwicmVwbHlfdG9fbWVzc2FnZV9pZCIsIm1lc3NhZ2VfaWQiLCJtZWRpYSIsInB1c2giLCJTVUJfQU5ZIiwiU1VCX0NBTkNFTCIsImJvdCIsInNlbmRNZXNzYWdlIiwiY2hhdCIsImlkIiwicmVwbHlfbWFya3VwIiwiZWRpdE1lc3NhZ2UiLCJwYXJhbXMiLCJfcGFyYW1zIiwicGFyc2VfbW9kZSIsImVkaXRNZXNzYWdlVGV4dCIsImVkaXRDdXJyZW50TWVzc2FnZSIsImNoYXRfaWQiLCJzZW5kQ3VycmVudE1lc3NhZ2UiLCJjaGF0SWQiLCJmb3J3YXJkTWVzc2FnZSIsInJlcGx5X3RvX21lc3NhZ2UiLCJ0eXBlIiwiY29uZGl0aW9uIiwic3VicyIsImdldE1zZ0NvbmRpdGlvbiIsIm9uZSIsImZ3ZE1zZyIsInJlc3BNc2ciLCJtZWRpYV9ncm91cF9pZCIsInNlbmRNZWRpYUdyb3VwIiwiY29uZmlnIiwiR3JvdXAiLCJ1cGRhdGUiLCJmd2RNc2dHcm91cElkIiwic3ViX3R5cGUiLCJmd2RNc2dJZCIsImFza0FkbWluIiwiZ2V0QWRtaW5BY3Rpb24iLCJmcm9tIiwiZGlzYWJsZV93ZWJfcGFnZV9wcmV2aWV3IiwicmVzaXplX2tleWJvYXJkIiwiYWN0aW9uTXNnIiwiYWN0aW9uTXNnSWQiLCJzZW5kQ2hhbm5lbCIsInJlc3AiLCJjYXB0aW9uIiwiZ2V0Q2FwdGlvbiIsIm9wdGlvbnMiLCJnZXRPcHRpb25zIiwiQ2hhbm5lbCIsImF1ZGlvIiwic2VuZEF1ZGlvIiwiZmlsZV9pZCIsImRvY3VtZW50Iiwic2VuZERvY3VtZW50Iiwidm9pY2UiLCJzZW5kVm9pY2UiLCJ2aWRlbyIsInNlbmRWaWRlbyIsInBob3RvIiwic2VuZFBob3RvIiwicmVjZWl2ZU1lc3NhZ2UiLCJyZWNlaXZlIiwicmVjZWl2ZV9kYXRlIiwiaGVscGVyIiwiaXNNdXRlIiwiZ2V0VGltZXN0YW1wIiwicmVjZWl2ZV9wYXJhbXMiLCJnZXRBZG1pbkFjdGlvbkZpbmlzaCIsImdldFJlcGx5dG9NZXNzYWdlSWQiLCJyZWplY3RNZXNzYWdlIiwicmVqZWN0IiwicmVhc29uIiwicmVqZWN0X2RhdGUiLCJyZWplY3RfcmVhc29uIiwicmVqZWN0VGV4dCIsImdldEFkbWluQWN0aW9uUmVqZWN0IiwicmVwbHlNZXNzYWdlIiwiY29tbWVudCIsInJlTW9kZSIsInJlIiwic3RhcnQiLCJyZXBseU1lc3NhZ2VXaXRoQ29tbWFuZCIsIm1zZyIsIm1hdGNoIiwicmVwIiwicmVwTXNnIiwiY29tbWFuZCIsImlzUHJpdmF0ZSIsImdldE1zZ1dpdGhSZXBseSIsImZvcndhcmRfZnJvbSIsImNoYXRNb2RlIiwic2xlZXAiLCJkZWxldGVNZXNzYWdlIiwicXVlcnkiLCJnZXRGd2RNc2dDb25kaXRpb24iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBOzs7O0FBRUE7Ozs7a0JBS0E7QUFDRTs7Ozs7QUFLQUEsUUFORixrQkFNVUMsT0FOVixFQU1tQjtBQUNmLFFBQUlDLFdBQVdELFFBQVFFLFlBQVIsR0FBdUJDLFdBQUtDLEdBQUwsQ0FBUyxVQUFULENBQXZCLEdBQThDRCxXQUFLQyxHQUFMLENBQVMsS0FBVCxDQUE3RDtBQUNBLFFBQUlDLGtCQUFrQixDQUFDLENBQUMsRUFBQ0MsTUFBTUwsUUFBUCxFQUFpQk0sZUFBZUMsV0FBS0MsUUFBckMsRUFBRCxDQUFELENBQXRCO0FBQ0EsUUFBSUMsc0JBQXVCVixRQUFRVyxVQUFULEdBQXVCWCxRQUFRVyxVQUEvQixHQUE0Q1gsUUFBUVksS0FBUixDQUFjLENBQWQsRUFBaUJELFVBQXZGO0FBQ0EsUUFBSUwsT0FBT0gsV0FBS0MsR0FBTCxDQUFTLGlCQUFULENBQVg7QUFDQSxRQUFJLENBQUNKLFFBQVFFLFlBQWIsRUFBMkI7QUFDekI7QUFDQUcsc0JBQWdCLENBQWhCLEVBQW1CUSxJQUFuQixDQUF3QixFQUFFUCxNQUFNSCxXQUFLQyxHQUFMLENBQVMsSUFBVCxDQUFSLEVBQXdCRyxlQUFlQyxXQUFLTSxPQUE1QyxFQUF4QjtBQUNELEtBSEQsTUFHTztBQUNMO0FBQ0FSLGFBQU9ILFdBQUtDLEdBQUwsQ0FBUyxxQkFBVCxDQUFQO0FBQ0Q7QUFDREMsb0JBQWdCUSxJQUFoQixDQUFxQixDQUFDLEVBQUVQLE1BQU1ILFdBQUtDLEdBQUwsQ0FBUyxtQkFBVCxDQUFSLEVBQXVDRyxlQUFlQyxXQUFLTyxVQUEzRCxFQUFELENBQXJCO0FBQ0FDLGNBQUlDLFdBQUosQ0FBZ0JqQixRQUFRa0IsSUFBUixDQUFhQyxFQUE3QixFQUFpQ2IsSUFBakMsRUFBdUM7QUFDckNJLDhDQURxQztBQUVyQ1Usb0JBQWMsRUFBRWYsZ0NBQUY7QUFGdUIsS0FBdkM7QUFJRCxHQXZCSDs7QUF3QkU7Ozs7OztBQU1BZ0IsYUE5QkYsdUJBOEJlZixJQTlCZixFQThCa0M7QUFBQSxRQUFiZ0IsTUFBYSx1RUFBSixFQUFJOztBQUM5QixRQUFJQyxVQUFVLHNCQUFjO0FBQzFCQyxrQkFBWTtBQURjLEtBQWQsRUFFWEYsTUFGVyxDQUFkO0FBR0EsV0FBT04sVUFBSVMsZUFBSixDQUFvQm5CLElBQXBCLEVBQTBCaUIsT0FBMUIsQ0FBUDtBQUNELEdBbkNIOztBQW9DRTs7Ozs7OztBQU9BRyxvQkEzQ0YsOEJBMkNzQnBCLElBM0N0QixFQTJDNEJOLE9BM0M1QixFQTJDa0Q7QUFBQSxRQUFic0IsTUFBYSx1RUFBSixFQUFJOztBQUM5QyxRQUFJQyxVQUFVLHNCQUFjO0FBQzFCSSxlQUFTM0IsUUFBUWtCLElBQVIsQ0FBYUMsRUFESTtBQUUxQlIsa0JBQVlYLFFBQVFXO0FBRk0sS0FBZCxFQUdYVyxNQUhXLENBQWQ7QUFJQSxXQUFPLEtBQUtELFdBQUwsQ0FBaUJmLElBQWpCLEVBQXVCaUIsT0FBdkIsQ0FBUDtBQUNELEdBakRIOztBQWtERTs7Ozs7OztBQU9BSyxvQkF6REYsOEJBeURzQnRCLElBekR0QixFQXlENEJOLE9BekQ1QixFQXlEa0Q7QUFBQSxRQUFic0IsTUFBYSx1RUFBSixFQUFJOztBQUM5QyxXQUFPLEtBQUtMLFdBQUwsQ0FBaUJqQixRQUFRa0IsSUFBUixDQUFhQyxFQUE5QixFQUFrQ2IsSUFBbEMsRUFBd0NnQixNQUF4QyxDQUFQO0FBQ0QsR0EzREg7O0FBNERFOzs7Ozs7O0FBT0FMLGFBbkVGLHVCQW1FZVksTUFuRWYsRUFtRXVCdkIsSUFuRXZCLEVBbUU2QmdCLE1BbkU3QixFQW1FcUM7QUFDakMsUUFBSUMsVUFBVSxzQkFBYztBQUMxQkMsa0JBQVk7QUFEYyxLQUFkLEVBRVhGLE1BRlcsQ0FBZDtBQUdBLFdBQU9OLFVBQUlDLFdBQUosQ0FBZ0JZLE1BQWhCLEVBQXdCdkIsSUFBeEIsRUFBOEJpQixPQUE5QixDQUFQO0FBQ0QsR0F4RUg7O0FBeUVFOzs7Ozs7O0FBT01PLGdCQWhGUiwwQkFnRndCQyxnQkFoRnhCLEVBZ0YwQ0MsSUFoRjFDLEVBZ0ZnRDtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUN4Q0MsdUJBRHdDLEdBQzVCQyxXQUFLQyxlQUFMLENBQXFCSixnQkFBckIsQ0FENEI7QUFFeEMvQixxQkFGd0MsR0FFOUJrQyxXQUFLRSxHQUFMLENBQVNILFNBQVQsQ0FGOEI7QUFHeENJLG9CQUh3QyxHQUcvQixFQUgrQixFQUczQkMsT0FIMkIsR0FHakIsRUFIaUI7QUFJNUM7O0FBSjRDLG1CQUt4Q3RDLFFBQVF1QyxjQUxnQztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBLHFCQU0xQnZCLFVBQUl3QixjQUFKLENBQW1CQyxhQUFPQyxLQUExQixFQUFpQzFDLFFBQVFZLEtBQXpDLENBTjBCOztBQUFBO0FBTTFDeUIsb0JBTjBDLGlCQU11QixDQU52Qjs7QUFPMUM7QUFDQUMsd0JBQVVKLFdBQUtTLE1BQUwsQ0FBWVYsU0FBWixFQUF1QixFQUFDVyxlQUFlUCxPQUFPRSxjQUF2QixFQUF1Q00sVUFBVWIsSUFBakQsRUFBdkIsQ0FBVjtBQVIwQztBQUFBOztBQUFBO0FBQUE7QUFBQSxxQkFXM0JoQixVQUFJYyxjQUFKLENBQW1CVyxhQUFPQyxLQUExQixFQUFpQzFDLFFBQVFrQixJQUFSLENBQWFDLEVBQTlDLEVBQWtEbkIsUUFBUVcsVUFBMUQsQ0FYMkI7O0FBQUE7QUFXMUMwQixvQkFYMEM7QUFXMkM7QUFDckZDLHdCQUFVSixXQUFLUyxNQUFMLENBQVlWLFNBQVosRUFBdUIsRUFBQ2EsVUFBVVQsT0FBTzFCLFVBQWxCLEVBQThCa0MsVUFBVWIsSUFBeEMsRUFBdkIsQ0FBVjs7QUFaMEM7QUFBQSwrQ0FjckMsRUFBQ3RCLHFCQUFxQjJCLE9BQU8xQixVQUE3QixFQUF5Q1gsU0FBU3NDLE9BQWxELEVBZHFDOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZTdDLEdBL0ZIOztBQWdHRTs7Ozs7QUFLTVMsVUFyR1IsMEJBcUdrRDtBQUFBOztBQUFBLFFBQS9CckMsbUJBQStCLFFBQS9CQSxtQkFBK0I7QUFBQSxRQUFWVixPQUFVLFFBQVZBLE9BQVU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDMUNpQyx1QkFEMEMsR0FDOUJDLFdBQUtDLGVBQUwsQ0FBcUJuQyxPQUFyQixDQUQ4QjtBQUUxQ00sa0JBRjBDLEdBRW5DSCxXQUFLNkMsY0FBTCxDQUFvQmhELE9BQXBCLENBRm1DO0FBRzFDaUQsa0JBSDBDLEdBR25DakQsUUFBUTZDLFFBQVIsSUFBb0JyQyxXQUFLTSxPQUF6QixHQUFtQyxXQUFuQyxHQUFpRCxNQUhkO0FBQUE7QUFBQSxxQkFJeEJFLFVBQUlDLFdBQUosQ0FBZ0J3QixhQUFPQyxLQUF2QixFQUE4QnBDLElBQTlCLEVBQW9DO0FBQ3hESSx3REFEd0Q7QUFFeERjLDRCQUFZLFVBRjRDO0FBR3hEMEIsMENBQTBCLElBSDhCO0FBSXhEOUIsOEJBQWM7QUFDWitCLG1DQUFpQixJQURMO0FBRVo5QyxtQ0FBaUIsQ0FBQyxDQUFDLEVBQUNDLE1BQU1ILFdBQUtDLEdBQUwsQ0FBUyxnQkFBVCxDQUFQLEVBQW1DRyw0QkFBMEIwQyxJQUE3RCxFQUFELENBQUQ7QUFGTDtBQUowQyxlQUFwQyxDQUp3Qjs7QUFBQTtBQUkxQ0csdUJBSjBDOztBQWE5Q2xCLHlCQUFLUyxNQUFMLENBQVlWLFNBQVosRUFBdUIsRUFBQ29CLGFBQWFELFVBQVV6QyxVQUF4QixFQUF2QixFQWI4QyxDQWFjOztBQWJkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBYy9DLEdBbkhIOzs7QUFxSEU7Ozs7OztBQU1NMkMsYUEzSFIsdUJBMkhxQnRELE9BM0hyQixFQTJIOEJzQixNQTNIOUIsRUEySHNDO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQzlCaUMsa0JBRDhCLEdBQ3ZCLElBRHVCO0FBRTlCQyxxQkFGOEIsR0FFcEJ0QixXQUFLdUIsVUFBTCxDQUFnQnpELE9BQWhCLEVBQXlCc0IsTUFBekIsQ0FGb0I7QUFHOUJvQyxxQkFIOEIsR0FHcEJ4QixXQUFLeUIsVUFBTCxDQUFnQjNELE9BQWhCLEVBQXlCd0QsT0FBekIsRUFBa0NsQyxNQUFsQyxDQUhvQjs7QUFBQSxtQkFJOUJ0QixRQUFRdUMsY0FKc0I7QUFBQTtBQUFBO0FBQUE7O0FBS2hDdkMsc0JBQVFZLEtBQVIsQ0FBYyxDQUFkLEVBQWlCNEMsT0FBakIsR0FBMkJBLE9BQTNCO0FBTGdDO0FBQUEscUJBTW5CeEMsVUFBSXdCLGNBQUosQ0FBbUJDLGFBQU9tQixPQUExQixFQUFtQzVELFFBQVFZLEtBQTNDLENBTm1COztBQUFBO0FBTWhDMkMsa0JBTmdDO0FBQUE7QUFBQTs7QUFBQTtBQUFBLG1CQU92QnZELFFBQVE2RCxLQVBlO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUEscUJBUW5CN0MsVUFBSThDLFNBQUosQ0FBY3JCLGFBQU9tQixPQUFyQixFQUE4QjVELFFBQVE2RCxLQUFSLENBQWNFLE9BQTVDLEVBQXFETCxPQUFyRCxDQVJtQjs7QUFBQTtBQVFoQ0gsa0JBUmdDO0FBQUE7QUFBQTs7QUFBQTtBQUFBLG1CQVN2QnZELFFBQVFnRSxRQVRlO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUEscUJBVW5CaEQsVUFBSWlELFlBQUosQ0FBaUJ4QixhQUFPbUIsT0FBeEIsRUFBaUM1RCxRQUFRZ0UsUUFBUixDQUFpQkQsT0FBbEQsRUFBMkRMLE9BQTNELENBVm1COztBQUFBO0FBVWhDSCxrQkFWZ0M7QUFBQTtBQUFBOztBQUFBO0FBQUEsbUJBV3ZCdkQsUUFBUWtFLEtBWGU7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQSxxQkFZbkJsRCxVQUFJbUQsU0FBSixDQUFjMUIsYUFBT21CLE9BQXJCLEVBQThCNUQsUUFBUWtFLEtBQVIsQ0FBY0gsT0FBNUMsRUFBcURMLE9BQXJELENBWm1COztBQUFBO0FBWWhDSCxrQkFaZ0M7QUFBQTtBQUFBOztBQUFBO0FBQUEsbUJBYXZCdkQsUUFBUW9FLEtBYmU7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQSxxQkFjbkJwRCxVQUFJcUQsU0FBSixDQUFjNUIsYUFBT21CLE9BQXJCLEVBQThCNUQsUUFBUW9FLEtBQVIsQ0FBY0wsT0FBNUMsRUFBcURMLE9BQXJELENBZG1COztBQUFBO0FBY2hDSCxrQkFkZ0M7QUFBQTtBQUFBOztBQUFBO0FBQUEsbUJBZXZCdkQsUUFBUXNFLEtBZmU7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQSxxQkFnQm5CdEQsVUFBSXVELFNBQUosQ0FBYzlCLGFBQU9tQixPQUFyQixFQUE4QjVELFFBQVFzRSxLQUFSLENBQWMsQ0FBZCxFQUFpQlAsT0FBL0MsRUFBd0RMLE9BQXhELENBaEJtQjs7QUFBQTtBQWdCaENILGtCQWhCZ0M7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQSxxQkFrQm5CdkMsVUFBSUMsV0FBSixDQUFnQndCLGFBQU9tQixPQUF2QixFQUFnQ0osT0FBaEMsRUFBeUNFLE9BQXpDLENBbEJtQjs7QUFBQTtBQWtCaENILGtCQWxCZ0M7O0FBQUE7QUFBQSxnREFvQjNCQSxJQXBCMkI7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFxQm5DLEdBaEpIOztBQWlKRTs7Ozs7OztBQU9NaUIsZ0JBeEpSLDBCQXdKd0J4RSxPQXhKeEIsRUF3SmlDeUUsT0F4SmpDLEVBd0p1RDtBQUFBOztBQUFBLFFBQWJuRCxNQUFhLHVFQUFKLEVBQUk7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFFL0N0QixRQUFRMEUsWUFGdUM7QUFBQTtBQUFBO0FBQUE7O0FBQUEsZ0RBRzFDMUQsVUFBSUMsV0FBSixDQUFnQndCLGFBQU9DLEtBQXZCLEVBQThCdkMsV0FBS0MsR0FBTCxDQUFTLFlBQVQsQ0FBOUIsRUFBc0Q7QUFDM0RNLHFDQUFxQlYsUUFBUThDO0FBRDhCLGVBQXRELENBSDBDOztBQUFBO0FBT25ELGtCQUFJNkIsYUFBT0MsTUFBUCxFQUFKLEVBQXFCO0FBQUN0RCx1QkFBT3NELE1BQVAsR0FBZ0IsSUFBaEI7QUFBcUI7QUFQUTtBQUFBLHFCQVFsQyxPQUFLdEIsV0FBTCxDQUFpQnRELE9BQWpCLEVBQTBCc0IsTUFBMUIsQ0FSa0M7O0FBQUE7QUFRL0NpQyxrQkFSK0M7QUFTL0N0Qix1QkFUK0MsR0FTbkNDLFdBQUtDLGVBQUwsQ0FBcUJuQyxPQUFyQixDQVRtQztBQVVuRDs7QUFDQUEsd0JBQVVrQyxXQUFLUyxNQUFMLENBQVlWLFNBQVosRUFBdUIsRUFBRXdDLGdCQUFGLEVBQVdDLGNBQWNDLGFBQU9FLFlBQVAsRUFBekIsRUFBZ0RDLGdCQUFnQnhELE1BQWhFLEVBQXZCLENBQVY7QUFDQTtBQUNJaEIsa0JBYitDLEdBYXhDSCxXQUFLNEUsb0JBQUwsQ0FBMEIvRSxPQUExQixDQWJ3QztBQWNuRDs7QUFkbUQ7QUFBQSxxQkFlN0MsT0FBS3FCLFdBQUwsQ0FBaUJmLElBQWpCLEVBQXVCLEVBQUNxQixTQUFTYyxhQUFPQyxLQUFqQixFQUF3Qi9CLFlBQVlYLFFBQVFxRCxXQUE1QyxFQUF5REgsMEJBQTBCLElBQW5GLEVBQXZCLENBZjZDOztBQUFBO0FBZ0IvQ3hDLGlDQWhCK0MsR0FnQnpCd0IsV0FBSzhDLG1CQUFMLENBQXlCaEYsT0FBekIsQ0FoQnlCO0FBaUJuRDs7QUFqQm1EO0FBQUEscUJBa0I3Q2dCLFVBQUlDLFdBQUosQ0FBZ0JqQixRQUFRa0IsSUFBUixDQUFhQyxFQUE3QixFQUFpQ2hCLFdBQUtDLEdBQUwsQ0FBUyxnQkFBVCxDQUFqQyxFQUE2RCxFQUFFTSx3Q0FBRixFQUE3RCxDQWxCNkM7O0FBQUE7QUFBQSxnREFtQjVDNkMsSUFuQjRDOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBb0JwRCxHQTVLSDs7QUE2S0U7Ozs7Ozs7QUFPTTBCLGVBcExSLHlCQW9MdUJqRixPQXBMdkIsRUFvTGdDa0YsTUFwTGhDLEVBb0x3Q0MsTUFwTHhDLEVBb0xnRDtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUV4Q25GLFFBQVFvRixXQUZnQztBQUFBO0FBQUE7QUFBQTs7QUFBQSxnREFHbkNwRSxVQUFJQyxXQUFKLENBQWdCd0IsYUFBT0MsS0FBdkIsRUFBOEJ2QyxXQUFLQyxHQUFMLENBQVMsbUJBQVQsQ0FBOUIsRUFBNkQ7QUFDbEVNLHFDQUFxQlYsUUFBUThDO0FBRHFDLGVBQTdELENBSG1DOztBQUFBO0FBT3hDYix1QkFQd0MsR0FPNUJDLFdBQUtDLGVBQUwsQ0FBcUJuQyxPQUFyQixDQVA0QjtBQVE1Qzs7QUFDQUEsd0JBQVVrQyxXQUFLUyxNQUFMLENBQVlWLFNBQVosRUFBdUIsRUFBRWlELGNBQUYsRUFBVUUsYUFBYVQsYUFBT0UsWUFBUCxFQUF2QixFQUE4Q1EsZUFBZUYsTUFBN0QsRUFBdkIsQ0FBVjtBQUNJRyx3QkFWd0MsR0FVM0JuRixXQUFLQyxHQUFMLENBQVMsYUFBVCxFQUF3QixFQUFFK0UsY0FBRixFQUF4QixDQVYyQjtBQVc1Qzs7QUFDSTdFLGtCQVp3QyxHQVlqQ0gsV0FBS29GLG9CQUFMLENBQTBCdkYsT0FBMUIsRUFBbUNtRixNQUFuQyxDQVppQztBQWE1Qzs7QUFDSXpFLGlDQWR3QyxHQWNsQndCLFdBQUs4QyxtQkFBTCxDQUF5QmhGLE9BQXpCLENBZGtCO0FBQUE7QUFBQSxxQkFldEMsT0FBS3FCLFdBQUwsQ0FBaUJmLElBQWpCLEVBQXVCLEVBQUNxQixTQUFTYyxhQUFPQyxLQUFqQixFQUF3Qi9CLFlBQVlYLFFBQVFxRCxXQUE1QyxFQUF5REgsMEJBQTBCLElBQW5GLEVBQXZCLENBZnNDOztBQUFBO0FBQUE7QUFBQSxxQkFnQnRDbEMsVUFBSUMsV0FBSixDQUFnQmpCLFFBQVFrQixJQUFSLENBQWFDLEVBQTdCLEVBQWlDbUUsVUFBakMsRUFBNkMsRUFBRTVFLHdDQUFGLEVBQXVCYyxZQUFZLFVBQW5DLEVBQTdDLENBaEJzQzs7QUFBQTtBQUFBLGdEQWlCckN4QixPQWpCcUM7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFrQjdDLEdBdE1IOztBQXVNRTs7Ozs7OztBQU9Nd0YsY0E5TVIsd0JBOE1zQnhGLE9BOU10QixFQThNK0J5RixPQTlNL0IsRUE4TXVEO0FBQUE7O0FBQUEsUUFBZkMsTUFBZSx1RUFBTixJQUFNO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUMvQ0EsTUFEK0M7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQSxxQkFFM0NDLFNBQUdDLEtBQUgsQ0FBUzVGLE9BQVQsQ0FGMkM7O0FBQUE7QUFBQTtBQUFBLHFCQUk3QyxPQUFLNEIsa0JBQUwsQ0FBd0J6QixXQUFLQyxHQUFMLENBQVMsWUFBVCxFQUF1QixFQUFFcUYsZ0JBQUYsRUFBdkIsQ0FBeEIsRUFBNkR6RixPQUE3RCxDQUo2Qzs7QUFBQTtBQUFBLGdEQUs1QyxJQUw0Qzs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1wRCxHQXBOSDs7QUFxTkU7Ozs7Ozs7Ozs7QUFVTTZGLHlCQS9OUiwwQ0ErTitFO0FBQUE7O0FBQUEsUUFBNUNDLEdBQTRDLFNBQTVDQSxHQUE0QztBQUFBLFFBQXZDQyxLQUF1QyxTQUF2Q0EsS0FBdUM7QUFBQSxRQUFoQ0MsR0FBZ0MsU0FBaENBLEdBQWdDO0FBQUEsUUFBM0JDLE1BQTJCLFNBQTNCQSxNQUEyQjtBQUFBLFFBQWpCQyxPQUFpQix1RUFBUCxLQUFPO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBQ3ZFdkIsYUFBT3dCLFNBQVAsQ0FBaUJMLEdBQWpCLENBRHVFO0FBQUE7QUFBQTtBQUFBOztBQUFBLGdEQUN2QyxLQUR1Qzs7QUFBQTtBQUVyRUwscUJBRnFFLEdBRTNETSxNQUFNLENBQU4sQ0FGMkQ7O0FBQUEsa0JBR3RFTixPQUhzRTtBQUFBO0FBQUE7QUFBQTs7QUFBQSxvQkFHdEQsRUFBQ3pGLFNBQVNHLFdBQUtDLEdBQUwsQ0FBUyxpQkFBVCxFQUE0QixFQUFFOEYsZ0JBQUYsRUFBNUIsQ0FBVixFQUhzRDs7QUFBQTtBQUdGO0FBQ3JFbEcscUJBSnVFLEdBSTdEa0MsV0FBS2tFLGVBQUwsQ0FBcUJILE1BQXJCLENBSjZEOztBQUFBLG9CQUt2RSxDQUFDakcsT0FBRCxJQUFZLENBQUNpRyxPQUFPSSxZQUxtRDtBQUFBO0FBQUE7QUFBQTs7QUFBQSxnREFLNUIsS0FMNEI7O0FBQUE7QUFLckI7QUFDdEQsa0JBQUksQ0FBQ3JHLE9BQUwsRUFBYztBQUFFQSwwQkFBVSxFQUFFa0IsTUFBTStFLE9BQU9JLFlBQWYsRUFBNkJwRCxNQUFNZ0QsT0FBT0ksWUFBMUMsRUFBVjtBQUFvRTtBQUNoRkMsc0JBUHVFLEdBTzVESixXQUFXLEtBQVgsR0FBbUIsSUFBbkIsR0FBMEIsS0FQa0M7QUFBQTtBQUFBLHFCQVFyRSxPQUFLVixZQUFMLENBQWtCeEYsT0FBbEIsRUFBMkJ5RixPQUEzQixFQUFvQ2EsUUFBcEMsQ0FScUU7O0FBQUE7QUFBQTtBQUFBLHFCQVN2RE4sSUFBSTdGLFdBQUtDLEdBQUwsQ0FBUyxpQkFBVCxDQUFKLENBVHVEOztBQUFBO0FBU3ZFa0MscUJBVHVFO0FBQUE7QUFBQSxxQkFVckVxQyxhQUFPNEIsS0FBUCxDQUFhLElBQWIsQ0FWcUU7O0FBQUE7QUFXM0UscUJBQUs3RSxrQkFBTCxDQUF3QixLQUF4QixFQUErQlksT0FBL0I7QUFYMkU7QUFBQSxxQkFZckVxQyxhQUFPNEIsS0FBUCxDQUFhLElBQWIsQ0FacUU7O0FBQUE7QUFhM0V2Rix3QkFBSXdGLGFBQUosQ0FBa0JsRSxRQUFRcEIsSUFBUixDQUFhQyxFQUEvQixFQUFtQ21CLFFBQVEzQixVQUEzQzs7QUFiMkU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFjNUUsR0E3T0g7O0FBOE9FOzs7OztBQUtNOEQsU0FuUFIsbUJBbVBpQmdDLEtBblBqQixFQW1Qd0I7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDaEJwRSxvQkFEZ0IsR0FDUG9FLE1BQU16RyxPQUFOLENBQWMrQixnQkFEUCxFQUN3Qjs7QUFDeENFLHVCQUZnQixHQUVKQyxXQUFLd0Usa0JBQUwsQ0FBd0JyRSxNQUF4QixDQUZJLEVBRTRCOztBQUM1Q3JDLHFCQUhnQixHQUdOa0MsV0FBS0UsR0FBTCxDQUFTSCxTQUFULENBSE0sRUFHYzs7QUFDbEMscUJBQUt1QyxjQUFMLENBQW9CeEUsT0FBcEIsRUFBNkJ5RyxNQUFNeEQsSUFBbkM7O0FBSm9CO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS3JCO0FBeFBILEMiLCJmaWxlIjoibXNnQ29udHJvbC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Y29uZmlnLCBib3QsIHZhcnMsIGxhbmcsIHN1YnMsIGhlbHBlciwgcmV9IGZyb20gJy4uL2NvcmUnO1xuXG4vKipcbiAqICjkuLvliqgp5raI5oGv5Y+R6YCB5o6n5Yi25ZmoXG4gKiBAdHlwZSB7QXJyYXl9XG4gKi9cbmV4cG9ydCBkZWZhdWx0XG57XG4gIC8qKlxuICAgKiDlkJHnlKjmiLflj5HpgIHmipXnqL/noa7orqTkv6Hmga9cbiAgICogQHBhcmFtICB7T2JqZWN0fSBtZXNzYWdlIE1lc3NhZ2VcbiAgICogQHJldHVybiB7W3R5cGVdfSAgICAgICAgIFtkZXNjcmlwdGlvbl1cbiAgICovXG4gIHN1YkFzayAobWVzc2FnZSkge1xuICAgIGxldCB5ZXNsYWJlbCA9IG1lc3NhZ2UuZm9yd2FyZF9kYXRlID8gbGFuZy5nZXQoJ3llc19vbmx5JykgOiBsYW5nLmdldCgneWVzJyk7XG4gICAgbGV0IGlubGluZV9rZXlib2FyZCA9IFtbe3RleHQ6IHllc2xhYmVsLCBjYWxsYmFja19kYXRhOiB2YXJzLlNVQl9SRUFMfV1dO1xuICAgIGxldCByZXBseV90b19tZXNzYWdlX2lkID0gKG1lc3NhZ2UubWVzc2FnZV9pZCkgPyBtZXNzYWdlLm1lc3NhZ2VfaWQgOiBtZXNzYWdlLm1lZGlhWzBdLm1lc3NhZ2VfaWQ7XG4gICAgbGV0IHRleHQgPSBsYW5nLmdldCgnc3ViX2NvbmZpcm1fdGlwJyk7XG4gICAgaWYgKCFtZXNzYWdlLmZvcndhcmRfZGF0ZSkge1xuICAgICAgLy8g5aaC5p6c5piv6L2s5Y+R55qE6K6v5oGv77yM5YiZ5oqV56i/6ICF5peg5p2D6YCJ5oup5Yy/5ZCNXG4gICAgICBpbmxpbmVfa2V5Ym9hcmRbMF0ucHVzaCh7IHRleHQ6IGxhbmcuZ2V0KCdubycpLCBjYWxsYmFja19kYXRhOiB2YXJzLlNVQl9BTlkgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIOaKleeov+iAhei9rOWPkeWIq+WkhOeahOa2iOaBr++8jOS4jeaYvuekuuWQpuaMiemSru+8jOW5tuS4lOaWh+ahiOS5n+acieaJgOS4jeWQjFxuICAgICAgdGV4dCA9IGxhbmcuZ2V0KCdzdWJfY29uZmlybV90aXBfZndkJyk7XG4gICAgfVxuICAgIGlubGluZV9rZXlib2FyZC5wdXNoKFt7IHRleHQ6IGxhbmcuZ2V0KCdzdWJfYnV0dG9uX2NhbmNlbCcpLCBjYWxsYmFja19kYXRhOiB2YXJzLlNVQl9DQU5DRUwgfV0pO1xuICAgIGJvdC5zZW5kTWVzc2FnZShtZXNzYWdlLmNoYXQuaWQsIHRleHQsIHtcbiAgICAgIHJlcGx5X3RvX21lc3NhZ2VfaWQsXG4gICAgICByZXBseV9tYXJrdXA6IHsgaW5saW5lX2tleWJvYXJkIH1cbiAgICB9KVxuICB9LFxuICAvKipcbiAgICog57yW6L6R5LiA5p2h5L+h5oGvXG4gICAqIEBwYXJhbSAge1t0eXBlXX0gdGV4dCAgICBbZGVzY3JpcHRpb25dXG4gICAqIEBwYXJhbSAge1t0eXBlXX0gcGFyYW1zICB7bWVzc2FnZV9pZCwgY2hhdF9pZH1cbiAgICogQHJldHVybiB7W3R5cGVdfSAgICAgICAgIFtkZXNjcmlwdGlvbl1cbiAgICovXG4gIGVkaXRNZXNzYWdlICh0ZXh0LCBwYXJhbXMgPSB7fSkge1xuICAgIGxldCBfcGFyYW1zID0gT2JqZWN0LmFzc2lnbih7XG4gICAgICBwYXJzZV9tb2RlOiAnTWFya2Rvd24nLFxuICAgIH0sIHBhcmFtcylcbiAgICByZXR1cm4gYm90LmVkaXRNZXNzYWdlVGV4dCh0ZXh0LCBfcGFyYW1zKTtcbiAgfSxcbiAgLyoqXG4gICAqIOe8lui+keW9k+WJjeeahOa2iOaBr1xuICAgKiBAcGFyYW0gIHtbdHlwZV19IHRleHQgICAgW2Rlc2NyaXB0aW9uXVxuICAgKiBAcGFyYW0gIHtbdHlwZV19IG1lc3NhZ2UgW2Rlc2NyaXB0aW9uXVxuICAgKiBAcGFyYW0gIHtPYmplY3R9IHBhcmFtcyAgW2Rlc2NyaXB0aW9uXVxuICAgKiBAcmV0dXJuIHtbdHlwZV19ICAgICAgICAgW2Rlc2NyaXB0aW9uXVxuICAgKi9cbiAgZWRpdEN1cnJlbnRNZXNzYWdlICh0ZXh0LCBtZXNzYWdlLCBwYXJhbXMgPSB7fSkge1xuICAgIGxldCBfcGFyYW1zID0gT2JqZWN0LmFzc2lnbih7XG4gICAgICBjaGF0X2lkOiBtZXNzYWdlLmNoYXQuaWQsXG4gICAgICBtZXNzYWdlX2lkOiBtZXNzYWdlLm1lc3NhZ2VfaWRcbiAgICB9LCBwYXJhbXMpO1xuICAgIHJldHVybiB0aGlzLmVkaXRNZXNzYWdlKHRleHQsIF9wYXJhbXMpO1xuICB9LFxuICAvKipcbiAgICog5L2/55So546w5pyJ57uT5p6E5Y+R6YCB5raI5oGvXG4gICAqIEBwYXJhbSAge1t0eXBlXX0gdGV4dCAgICBbZGVzY3JpcHRpb25dXG4gICAqIEBwYXJhbSAge1t0eXBlXX0gbWVzc2FnZSBbZGVzY3JpcHRpb25dXG4gICAqIEBwYXJhbSAge09iamVjdH0gcGFyYW1zICBbZGVzY3JpcHRpb25dXG4gICAqIEByZXR1cm4ge1t0eXBlXX0gICAgICAgICBbZGVzY3JpcHRpb25dXG4gICAqL1xuICBzZW5kQ3VycmVudE1lc3NhZ2UgKHRleHQsIG1lc3NhZ2UsIHBhcmFtcyA9IHt9KSB7XG4gICAgcmV0dXJuIHRoaXMuc2VuZE1lc3NhZ2UobWVzc2FnZS5jaGF0LmlkLCB0ZXh0LCBwYXJhbXMpO1xuICB9LFxuICAvKipcbiAgICog5Y+R6YCB5raI5oGvLOm7mOiupOS9v+eUqG1hcmtkb3duXG4gICAqIEBwYXJhbSAge1t0eXBlXX0gY2hhdElkIFtkZXNjcmlwdGlvbl1cbiAgICogQHBhcmFtICB7W3R5cGVdfSB0ZXh0ICAgW2Rlc2NyaXB0aW9uXVxuICAgKiBAcGFyYW0gIHtbdHlwZV19IHBhcmFtcyBbZGVzY3JpcHRpb25dXG4gICAqIEByZXR1cm4ge1t0eXBlXX0gICAgICAgIFtkZXNjcmlwdGlvbl1cbiAgICovXG4gIHNlbmRNZXNzYWdlIChjaGF0SWQsIHRleHQsIHBhcmFtcykge1xuICAgIGxldCBfcGFyYW1zID0gT2JqZWN0LmFzc2lnbih7XG4gICAgICBwYXJzZV9tb2RlOiAnTWFya2Rvd24nLFxuICAgIH0sIHBhcmFtcylcbiAgICByZXR1cm4gYm90LnNlbmRNZXNzYWdlKGNoYXRJZCwgdGV4dCwgX3BhcmFtcyk7XG4gIH0sXG4gIC8qKlxuICAgKiDlsIbmtojmga/ovazlj5HliLDlrqHnqL/nvqRcbiAgICogQHBhcmFtICB7T2JqZWN0fSAgcmVwbHlfdG9fbWVzc2FnZSBNZXNzYWdlXG4gICAqIEBwYXJhbSAge1N0cmluZ30gIHR5cGUgICAg5oqV56i/57G75Z6LXG4gICAqIEByZXR1cm4ge1Byb21pc2V9ICAgICAgICAg5oiQ5Yqf5ZCO6L+U5Zue6L2s5Y+R5ZCO55qETWVzc2FnZeWvueixoVxuICAgKiB7cmVwbHlfdG9fbWVzc2FnZV9pZDog5a6h56i/576kYWN0aW9uTXNn5bqU6K+l5Zue5aSN55qE56i/5Lu25raI5oGvSUTvvIxtZXNzYWdl77yM56i/5Lu2fVxuICAgKi9cbiAgYXN5bmMgZm9yd2FyZE1lc3NhZ2UgKHJlcGx5X3RvX21lc3NhZ2UsIHR5cGUpIHtcbiAgICBsZXQgY29uZGl0aW9uID0gc3Vicy5nZXRNc2dDb25kaXRpb24ocmVwbHlfdG9fbWVzc2FnZSk7XG4gICAgbGV0IG1lc3NhZ2UgPSBzdWJzLm9uZShjb25kaXRpb24pO1xuICAgIGxldCBmd2RNc2cgPSB7fSwgcmVzcE1zZyA9IHt9O1xuICAgIC8vIOiLpeaYr21lZGlhZ3JvdXDmtojmga9cbiAgICBpZiAobWVzc2FnZS5tZWRpYV9ncm91cF9pZCkge1xuICAgICAgZndkTXNnID0gKGF3YWl0IGJvdC5zZW5kTWVkaWFHcm91cChjb25maWcuR3JvdXAsIG1lc3NhZ2UubWVkaWEpKVswXTtcbiAgICAgIC8vIOWwhuWuoeeov+e+pOeahG1lZGlhZ3JvdXBJZOWGmeWIsG1lZGlhR3JvdXDmtojmga/nmoRmd2RNc2dHcm91cElk6IqC54K5XG4gICAgICByZXNwTXNnID0gc3Vicy51cGRhdGUoY29uZGl0aW9uLCB7ZndkTXNnR3JvdXBJZDogZndkTXNnLm1lZGlhX2dyb3VwX2lkLCBzdWJfdHlwZTogdHlwZX0pO1xuICAgIH0gZWxzZSB7XG4gICAgICAvLyDpmYTliqDlrqHnqL/nvqTnmoTmtojmga9JROWIsOeov+S7tlxuICAgICAgZndkTXNnID0gYXdhaXQgYm90LmZvcndhcmRNZXNzYWdlKGNvbmZpZy5Hcm91cCwgbWVzc2FnZS5jaGF0LmlkLCBtZXNzYWdlLm1lc3NhZ2VfaWQpOy8vIOi9rOWPkeiHs+Wuoeeov+e+pFxuICAgICAgcmVzcE1zZyA9IHN1YnMudXBkYXRlKGNvbmRpdGlvbiwge2Z3ZE1zZ0lkOiBmd2RNc2cubWVzc2FnZV9pZCwgc3ViX3R5cGU6IHR5cGV9KTtcbiAgICB9XG4gICAgcmV0dXJuIHtyZXBseV90b19tZXNzYWdlX2lkOiBmd2RNc2cubWVzc2FnZV9pZCwgbWVzc2FnZTogcmVzcE1zZ307XG4gIH0sXG4gIC8qKlxuICAgKiDor6Lpl67nrqHnkIblkZjlpoLkvZXlpITnkIbnqL/ku7ZcbiAgICogQHBhcmFtICB7T2JqZWN0fSB7cmVwbHlfdG9fbWVzc2FnZV9pZDog5a6h56i/576kYWN0aW9uTXNn5bqU6K+l5Zue5aSN55qE56i/5Lu25raI5oGvSUTvvIxtZXNzYWdl77yM56i/5Lu2fVxuICAgKiBAcmV0dXJuIHtbdHlwZV19ICAgICAgICAgW2Rlc2NyaXB0aW9uXVxuICAgKi9cbiAgYXN5bmMgYXNrQWRtaW4gKHtyZXBseV90b19tZXNzYWdlX2lkLCBtZXNzYWdlfSkge1xuICAgIGxldCBjb25kaXRpb24gPSBzdWJzLmdldE1zZ0NvbmRpdGlvbihtZXNzYWdlKTtcbiAgICBsZXQgdGV4dCA9IGxhbmcuZ2V0QWRtaW5BY3Rpb24obWVzc2FnZSk7XG4gICAgbGV0IGZyb20gPSBtZXNzYWdlLnN1Yl90eXBlID09IHZhcnMuU1VCX0FOWSA/ICdhbm9ueW1vdXMnIDogJ3JlYWwnO1xuICAgIGxldCBhY3Rpb25Nc2cgPSBhd2FpdCBib3Quc2VuZE1lc3NhZ2UoY29uZmlnLkdyb3VwLCB0ZXh0LCB7XG4gICAgICByZXBseV90b19tZXNzYWdlX2lkLFxuICAgICAgcGFyc2VfbW9kZTogJ01hcmtkb3duJyxcbiAgICAgIGRpc2FibGVfd2ViX3BhZ2VfcHJldmlldzogdHJ1ZSxcbiAgICAgIHJlcGx5X21hcmt1cDoge1xuICAgICAgICByZXNpemVfa2V5Ym9hcmQ6IHRydWUsXG4gICAgICAgIGlubGluZV9rZXlib2FyZDogW1t7dGV4dDogbGFuZy5nZXQoJ2J1dHRvbl9yZWNlaXZlJyksIGNhbGxiYWNrX2RhdGE6IGByZWNlaXZlOiR7ZnJvbX1gfV1dXG4gICAgICB9XG4gICAgfSk7XG4gICAgc3Vicy51cGRhdGUoY29uZGl0aW9uLCB7YWN0aW9uTXNnSWQ6IGFjdGlvbk1zZy5tZXNzYWdlX2lkfSk7Ly8g5pu05pawYWN0aW9uTXNnSWRcbiAgfSxcblxuICAvKipcbiAgICog5o6o6YCB6aKR6YGT5raI5oGvXG4gICAqIEBwYXJhbSAge1t0eXBlXX0gbWVzc2FnZSBbZGVzY3JpcHRpb25dXG4gICAqIEBwYXJhbSAge1t0eXBlXX0gcGFyYW1zICBjb21tZW50PeivhOiuuiwgaXNNdXRlPeaYr+WQpumdmemfs1xuICAgKiBAcmV0dXJuIHtbdHlwZV19ICAgICAgICAgW2Rlc2NyaXB0aW9uXVxuICAgKi9cbiAgYXN5bmMgc2VuZENoYW5uZWwgKG1lc3NhZ2UsIHBhcmFtcykge1xuICAgIGxldCByZXNwID0gbnVsbDtcbiAgICBsZXQgY2FwdGlvbiA9IHN1YnMuZ2V0Q2FwdGlvbihtZXNzYWdlLCBwYXJhbXMpO1xuICAgIGxldCBvcHRpb25zID0gc3Vicy5nZXRPcHRpb25zKG1lc3NhZ2UsIGNhcHRpb24sIHBhcmFtcyk7XG4gICAgaWYgKG1lc3NhZ2UubWVkaWFfZ3JvdXBfaWQpIHtcbiAgICAgIG1lc3NhZ2UubWVkaWFbMF0uY2FwdGlvbiA9IGNhcHRpb247XG4gICAgICByZXNwID0gYXdhaXQgYm90LnNlbmRNZWRpYUdyb3VwKGNvbmZpZy5DaGFubmVsLCBtZXNzYWdlLm1lZGlhKTtcbiAgICB9IGVsc2UgaWYgKG1lc3NhZ2UuYXVkaW8pIHtcbiAgICAgIHJlc3AgPSBhd2FpdCBib3Quc2VuZEF1ZGlvKGNvbmZpZy5DaGFubmVsLCBtZXNzYWdlLmF1ZGlvLmZpbGVfaWQsIG9wdGlvbnMpO1xuICAgIH0gZWxzZSBpZiAobWVzc2FnZS5kb2N1bWVudCkge1xuICAgICAgcmVzcCA9IGF3YWl0IGJvdC5zZW5kRG9jdW1lbnQoY29uZmlnLkNoYW5uZWwsIG1lc3NhZ2UuZG9jdW1lbnQuZmlsZV9pZCwgb3B0aW9ucyk7XG4gICAgfSBlbHNlIGlmIChtZXNzYWdlLnZvaWNlKSB7XG4gICAgICByZXNwID0gYXdhaXQgYm90LnNlbmRWb2ljZShjb25maWcuQ2hhbm5lbCwgbWVzc2FnZS52b2ljZS5maWxlX2lkLCBvcHRpb25zKTtcbiAgICB9IGVsc2UgaWYgKG1lc3NhZ2UudmlkZW8pIHtcbiAgICAgIHJlc3AgPSBhd2FpdCBib3Quc2VuZFZpZGVvKGNvbmZpZy5DaGFubmVsLCBtZXNzYWdlLnZpZGVvLmZpbGVfaWQsIG9wdGlvbnMpO1xuICAgIH0gZWxzZSBpZiAobWVzc2FnZS5waG90bykge1xuICAgICAgcmVzcCA9IGF3YWl0IGJvdC5zZW5kUGhvdG8oY29uZmlnLkNoYW5uZWwsIG1lc3NhZ2UucGhvdG9bMF0uZmlsZV9pZCwgb3B0aW9ucyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJlc3AgPSBhd2FpdCBib3Quc2VuZE1lc3NhZ2UoY29uZmlnLkNoYW5uZWwsIGNhcHRpb24sIG9wdGlvbnMpIFxuICAgIH1cbiAgICByZXR1cm4gcmVzcDtcbiAgfSxcbiAgLyoqXG4gICAqIOWuoeaguOeov+S7tlxuICAgKiBAcGFyYW0gIHtPYmplY3R9IG1lc3NhZ2UgICAgICAgICAgICAg56i/5Lu277yM5p+l6K+i5Ye65p2l55qEXG4gICAqIEBwYXJhbSAge09iamVjdH0gcmVjZWl2ZSAgICAgICAgICAgICDlrqHnqL/kurrlr7nosaHvvIzkuIDoiKzmmK9tZXNzYWdlLmZyb21cbiAgICogQHBhcmFtICB7U3RyaW5nfSBjb21tZW50ICAgICAgICAgICAgIOmZhOWKoOivhOiuulxuICAgKiBAcGFyYW0gIHtCb29sZWFufSBpc011dGUgICAgICAgICAgICAg5piv5ZCm6Z2Z6Z+z5o6o6YCBXG4gICAqL1xuICBhc3luYyByZWNlaXZlTWVzc2FnZSAobWVzc2FnZSwgcmVjZWl2ZSwgcGFyYW1zID0ge30pIHtcbiAgICAvLyDoi6XnqL/ku7blt7Lnu4/lj5HluIPvvIzliJnpqbPlm57mk43kvZxcbiAgICBpZiAobWVzc2FnZS5yZWNlaXZlX2RhdGUpIHsgXG4gICAgICByZXR1cm4gYm90LnNlbmRNZXNzYWdlKGNvbmZpZy5Hcm91cCwgbGFuZy5nZXQoJ2Vycl9yZXBlYXQnKSwge1xuICAgICAgICByZXBseV90b19tZXNzYWdlX2lkOiBtZXNzYWdlLmZ3ZE1zZ0lkXG4gICAgICB9KSBcbiAgICB9XG4gICAgaWYgKGhlbHBlci5pc011dGUoKSkge3BhcmFtcy5pc011dGUgPSB0cnVlfVxuICAgIGxldCByZXNwID0gYXdhaXQgdGhpcy5zZW5kQ2hhbm5lbChtZXNzYWdlLCBwYXJhbXMpO1xuICAgIGxldCBjb25kaXRpb24gPSBzdWJzLmdldE1zZ0NvbmRpdGlvbihtZXNzYWdlKTtcbiAgICAvLyDorrDlvZXlrqHnqL/kurrlkozml7bpl7RcbiAgICBtZXNzYWdlID0gc3Vicy51cGRhdGUoY29uZGl0aW9uLCB7IHJlY2VpdmUsIHJlY2VpdmVfZGF0ZTogaGVscGVyLmdldFRpbWVzdGFtcCgpLCByZWNlaXZlX3BhcmFtczogcGFyYW1zIH0pXG4gICAgLy8g6I635Y+W5a6h56i/576k6YCa6L+H5a6h5qC45paH5qGIXG4gICAgbGV0IHRleHQgPSBsYW5nLmdldEFkbWluQWN0aW9uRmluaXNoKG1lc3NhZ2UpO1xuICAgIC8vIOe8lui+keWuoeeov+e+pGFjdGlvbk1zZ1xuICAgIGF3YWl0IHRoaXMuZWRpdE1lc3NhZ2UodGV4dCwge2NoYXRfaWQ6IGNvbmZpZy5Hcm91cCwgbWVzc2FnZV9pZDogbWVzc2FnZS5hY3Rpb25Nc2dJZCwgZGlzYWJsZV93ZWJfcGFnZV9wcmV2aWV3OiB0cnVlfSk7XG4gICAgbGV0IHJlcGx5X3RvX21lc3NhZ2VfaWQgPSBzdWJzLmdldFJlcGx5dG9NZXNzYWdlSWQobWVzc2FnZSk7XG4gICAgLy8g5ZCR55So5oi35Y+R6YCB56i/5Lu26L+H5a6h5L+h5oGvXG4gICAgYXdhaXQgYm90LnNlbmRNZXNzYWdlKG1lc3NhZ2UuY2hhdC5pZCwgbGFuZy5nZXQoJ3N1Yl9maW5pc2hfdGlwJyksIHsgcmVwbHlfdG9fbWVzc2FnZV9pZCB9KVxuICAgIHJldHVybiByZXNwO1xuICB9LFxuICAvKipcbiAgICog5ouS57ud5oqV56i/XG4gICAqIEBwYXJhbSAge1t0eXBlXX0gbWVzc2FnZSBbZGVzY3JpcHRpb25dXG4gICAqIEBwYXJhbSAge09iamVjdH0gcmVqZWN0ICDmmK/osIHmk43kvZznmoRcbiAgICogQHBhcmFtICB7U3RyaW5nfSByZWFzb24gIOeQhueUsVxuICAgKiBAcmV0dXJuIHtbdHlwZV19ICAgICAgICAgW2Rlc2NyaXB0aW9uXVxuICAgKi9cbiAgYXN5bmMgcmVqZWN0TWVzc2FnZSAobWVzc2FnZSwgcmVqZWN0LCByZWFzb24pIHtcbiAgICAvLyDoi6XnqL/ku7blt7Lnu4/mi5Lnu53vvIzliJnpqbPlm55cbiAgICBpZiAobWVzc2FnZS5yZWplY3RfZGF0ZSkgeyBcbiAgICAgIHJldHVybiBib3Quc2VuZE1lc3NhZ2UoY29uZmlnLkdyb3VwLCBsYW5nLmdldCgnZXJyX3JlcGVhdF9yZWplY3QnKSwge1xuICAgICAgICByZXBseV90b19tZXNzYWdlX2lkOiBtZXNzYWdlLmZ3ZE1zZ0lkXG4gICAgICB9KSBcbiAgICB9XG4gICAgbGV0IGNvbmRpdGlvbiA9IHN1YnMuZ2V0TXNnQ29uZGl0aW9uKG1lc3NhZ2UpO1xuICAgIC8vIOiusOW9leaTjeS9nOS6uuWSjOaLkue7neeQhueUseWPiuaXtumXtFxuICAgIG1lc3NhZ2UgPSBzdWJzLnVwZGF0ZShjb25kaXRpb24sIHsgcmVqZWN0LCByZWplY3RfZGF0ZTogaGVscGVyLmdldFRpbWVzdGFtcCgpLCByZWplY3RfcmVhc29uOiByZWFzb24gfSlcbiAgICBsZXQgcmVqZWN0VGV4dCA9IGxhbmcuZ2V0KCdyZWplY3RfdGlwcycsIHsgcmVhc29uIH0pO1xuICAgIC8vIOiOt+WPluWuoeeov+e+pOaLkue7neWuoeaguOaWh+ahiFxuICAgIGxldCB0ZXh0ID0gbGFuZy5nZXRBZG1pbkFjdGlvblJlamVjdChtZXNzYWdlLCByZWFzb24pO1xuICAgIC8vIOe8lui+keWuoeeov+e+pGFjdGlvbk1zZ1xuICAgIGxldCByZXBseV90b19tZXNzYWdlX2lkID0gc3Vicy5nZXRSZXBseXRvTWVzc2FnZUlkKG1lc3NhZ2UpO1xuICAgIGF3YWl0IHRoaXMuZWRpdE1lc3NhZ2UodGV4dCwge2NoYXRfaWQ6IGNvbmZpZy5Hcm91cCwgbWVzc2FnZV9pZDogbWVzc2FnZS5hY3Rpb25Nc2dJZCwgZGlzYWJsZV93ZWJfcGFnZV9wcmV2aWV3OiB0cnVlfSlcbiAgICBhd2FpdCBib3Quc2VuZE1lc3NhZ2UobWVzc2FnZS5jaGF0LmlkLCByZWplY3RUZXh0LCB7IHJlcGx5X3RvX21lc3NhZ2VfaWQsIHBhcnNlX21vZGU6ICdNYXJrZG93bicgfSk7XG4gICAgcmV0dXJuIG1lc3NhZ2U7XG4gIH0sXG4gIC8qKlxuICAgKiDlm57lpI3nlKjmiLfmtojmga/vvIzlkIzml7bnlKjmiLflsIbov5vlhaXogYrlpKnnirbmgIFcbiAgICog55So5oi35Y+v6YCP6L+HS2V5Ym9hcmRCdXR0b27pgIDlh7rogYrlpKnvvIznrqHnkIblkZjlj6/pgI/ov4cvZW5kcmUg57uT5p2f5Lya6K+dXG4gICAqIEBwYXJhbSAge1t0eXBlXX0gbWVzc2FnZSDnqL/ku7ZcbiAgICogQHBhcmFtICB7U3RyaW5nfSBjb21tZW50ICDnrqHnkIblkZjlm57lpI3nu5nnlKjmiLfnmoTmtojmga9cbiAgICogQHJldHVybiB7W3R5cGVdfSAgICAgICAgIFtkZXNjcmlwdGlvbl1cbiAgICovXG4gIGFzeW5jIHJlcGx5TWVzc2FnZSAobWVzc2FnZSwgY29tbWVudCwgcmVNb2RlID0gdHJ1ZSkge1xuICAgIGlmIChyZU1vZGUpIHtcbiAgICAgIGF3YWl0IHJlLnN0YXJ0KG1lc3NhZ2UpOy8vIOi/m+WFpeS8muivneaooeW8j1xuICAgIH1cbiAgICBhd2FpdCB0aGlzLnNlbmRDdXJyZW50TWVzc2FnZShsYW5nLmdldCgncmVfY29tbWVudCcsIHsgY29tbWVudCB9KSwgbWVzc2FnZSk7XG4gICAgcmV0dXJuIHRydWU7XG4gIH0sXG4gIC8qKlxuICAgKiDlm57lpI3nlKjmiLfkv6Hmga9cbiAgICogQHBhcmFtICB7W3R5cGVdfSBvcHRpb25zLm1zZyAgICBbZGVzY3JpcHRpb25dXG4gICAqIEBwYXJhbSAge1t0eXBlXX0gb3B0aW9ucy5tYXRjaCAgW2Rlc2NyaXB0aW9uXVxuICAgKiBAcGFyYW0gIHtbdHlwZV19IG9wdGlvbnMucmVwICAgIFtkZXNjcmlwdGlvbl1cbiAgICogQHBhcmFtICB7W3R5cGVdfSBvcHRpb25zLnJlcE1zZyBbZGVzY3JpcHRpb25dXG4gICAqIEBwYXJhbSAge1N0cmluZ30gY29tbWFuZCAgICAgICAgL3JlIOaIluiAhSAvZWNobyBcbiAgICogcmUg5Lya6L+b5YWl5Lya6K+d54q25oCB77yMIGVjaG8g5Y+q5piv5Y+R6YCB77yM5LiN6L+b5YWl5Lya6K+dXG4gICAqIEByZXR1cm4ge1t0eXBlXX0gICAgICAgICAgICAgICAgW2Rlc2NyaXB0aW9uXVxuICAgKi9cbiAgYXN5bmMgcmVwbHlNZXNzYWdlV2l0aENvbW1hbmQgKHsgbXNnLCBtYXRjaCwgcmVwLCByZXBNc2cgfSwgY29tbWFuZCA9ICcvcmUnKSB7XG4gICAgaWYgKGhlbHBlci5pc1ByaXZhdGUobXNnKSkgeyByZXR1cm4gZmFsc2UgfVxuICAgIGNvbnN0IGNvbW1lbnQgPSBtYXRjaFsxXTtcbiAgICBpZiAoIWNvbW1lbnQpIHt0aHJvdyB7bWVzc2FnZTogbGFuZy5nZXQoJ2FkbWluX3JlcGx5X2VycicsIHsgY29tbWFuZCB9KX19Ly8g5rKh5pyJ6L6T5YWl5raI5oGvXG4gICAgbGV0IG1lc3NhZ2UgPSBzdWJzLmdldE1zZ1dpdGhSZXBseShyZXBNc2cpO1xuICAgIGlmICghbWVzc2FnZSAmJiAhcmVwTXNnLmZvcndhcmRfZnJvbSkgeyByZXR1cm4gZmFsc2UgfS8vIOaXoOS7juWbnuWkjVxuICAgIGlmICghbWVzc2FnZSkgeyBtZXNzYWdlID0geyBjaGF0OiByZXBNc2cuZm9yd2FyZF9mcm9tLCBmcm9tOiByZXBNc2cuZm9yd2FyZF9mcm9tIH0gfVxuICAgIGxldCBjaGF0TW9kZSA9IGNvbW1hbmQgPT0gJy9yZScgPyB0cnVlIDogZmFsc2U7XG4gICAgYXdhaXQgdGhpcy5yZXBseU1lc3NhZ2UobWVzc2FnZSwgY29tbWVudCwgY2hhdE1vZGUpO1xuICAgIGxldCByZXNwTXNnID0gYXdhaXQgcmVwKGxhbmcuZ2V0KCdyZV9zZW5kX3N1Y2Nlc3MnKSk7XG4gICAgYXdhaXQgaGVscGVyLnNsZWVwKDEwMDApO1xuICAgIHRoaXMuZWRpdEN1cnJlbnRNZXNzYWdlKFwiLi4uXCIsIHJlc3BNc2cpO1xuICAgIGF3YWl0IGhlbHBlci5zbGVlcCgyMDAwKTtcbiAgICBib3QuZGVsZXRlTWVzc2FnZShyZXNwTXNnLmNoYXQuaWQsIHJlc3BNc2cubWVzc2FnZV9pZCk7XG4gIH0sXG4gIC8qKlxuICAgKiDnrqHnkIblkZjngrnlh7vph4fnurPnqL/ku7Yo5LuOYWN0aW9uTXNn54K55Ye75oyJ6ZKuKVxuICAgKiBAcGFyYW0gIHtPYmplY3R9ICBxdWVyeSBjYWxsYmFjayBkYXRhXG4gICAqIEByZXR1cm4ge1Byb21pc2V9ICAgICAgIFtkZXNjcmlwdGlvbl1cbiAgICovXG4gIGFzeW5jIHJlY2VpdmUgKHF1ZXJ5KSB7XG4gICAgbGV0IGZ3ZE1zZyA9IHF1ZXJ5Lm1lc3NhZ2UucmVwbHlfdG9fbWVzc2FnZTsvLyDlrqHnqL/nvqTnmoTnqL/ku7ZcbiAgICBsZXQgY29uZGl0aW9uID0gc3Vicy5nZXRGd2RNc2dDb25kaXRpb24oZndkTXNnKTsvLyDlvpfliLDmn6Xor6LmnaHku7ZcbiAgICBsZXQgbWVzc2FnZSA9IHN1YnMub25lKGNvbmRpdGlvbik7Ly8g5b6X5Yiw55yf5a6e56i/5Lu2XG4gICAgdGhpcy5yZWNlaXZlTWVzc2FnZShtZXNzYWdlLCBxdWVyeS5mcm9tKTtcbiAgfVxufVxuIl19