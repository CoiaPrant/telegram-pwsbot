"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

var _core = require('../core');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * 用于翻译文案的工具
 */
var Lang = function () {
  function Lang(langName, vars) {
    (0, _classCallCheck3.default)(this, Lang);
    this.langName = null;
    this.lang = null;

    this.langName = langName;
    this.vars = vars;
  }

  (0, _createClass3.default)(Lang, [{
    key: "get",

    /**
     * 得到翻译内容
     * @param  {String} key     键名，对于Langjson
     * @param  {Object} context 包含变数的对象
     * @return {[type]}         [description]
     */
    value: function get(key, context) {
      var text = this.language[key];
      if (!text) {
        throw new Error("\u4E0D\u5B58\u5728\u7684Key\uFF0C\u8BED\u8A00\uFF1A" + this.langName + "\uFF0C\u952E\uFF1A" + key + "\uFF0C\u8BF7\u68C0\u67E5\u7FFB\u8BD1\u6587\u4EF6\uFF01");
      }
      return text.replace(/{{(.*?)}}/g, function (match, key) {
        return context[key.trim()];
      });
    }
  }, {
    key: "getAdminActionFinish",

    /**
     * 获取管理员准许投稿后，审稿群的actionMsg文案
     * @param  {Object} message Message   稿件
     * @return {[type]}         [description]
     */
    value: function getAdminActionFinish(message) {
      var text = this.getAdminCommonHeader(message);
      text += "\n" + this.getAdminreader(message);
      text += "\n" + this.get('admin_finish_label');
      var comment = this.getAdminComment(message);
      if (comment) {
        text += "\n\n" + this.get('admin_finish_comment', { comment: comment });
      }
      return text;
    }
  }, {
    key: "getAdminComment",

    /**
     * 获取管理员对消息的评语
     * @param  {[type]} message [description]
     * @return {string}         存在则返回，没有则返回空
     */
    value: function getAdminComment(message) {
      var params = message.receive_params;
      return params ? params.comment : false;
    }
  }, {
    key: "getAdminActionReject",
    value: function getAdminActionReject(message, reason) {
      var text = this.getAdminCommonHeader(message);
      text += "\n" + this.getAdminReject(message);
      text += "\n" + this.get('admin_reject_label', { reason: reason });
      return text;
    }
  }, {
    key: "getAdminReject",
    value: function getAdminReject(message) {
      var userinfo = this.getUser(message.reject);
      return this.get('admin_reject', userinfo);
    }
  }, {
    key: "getAdminreader",
    value: function getAdminreader(message) {
      var userinfo = this.getUser(message.receive);
      return this.get('admin_reader', userinfo);
    }
  }, {
    key: "getViaInfo",

    /**
     * 获取推送到频道内容的页脚版权文本
     * via xxxx
     * @param  {[type]} message [description]
     * @return {[type]}         [description]
     */
    value: function getViaInfo(message, showVia) {
      var msgInfo = this.getMessageFwdFromInfo(message);
      var text = '';

      text += '_'

      if (msgInfo.type == 'channel_private') {
        text += this.get('from_channel_private', msgInfo) + " ";
      } else if (msgInfo.type == 'channel') {
        text += this.get('from_channel', msgInfo) + " ";
      } else if (msgInfo.type == 'forward_user') {
        text += this.get('from_user', msgInfo) + " ";
      }

      if (showVia) {
        text += this.get('via_user', msgInfo);
      } else {
        text += this.get('via_anonymous', msgInfo);
      }

      text += '_'

      return text;
    }
  }, {
    key: "getUser",

    /**
     * 获取投稿人username, userid
     * @param  {[type]} message [description]
     * @return {Object}        {username, userid}
     */
    value: function getUser(user) {
      var lastName = user.last_name || '';
      var firstName = user.first_name || '';
      var username = _core.helper.formatText(firstName + ' ' + lastName);

      if (!username) {
        if (user.username) {
          username = user.username;
        } else {
          username = 'NoName';
        }
      }
      var userid = user.id;
      return { username: username, userid: userid };
    }
  }, {
    key: "getMessageFwdFromInfo",

    /**
     * 获取转发信息的来源
     * 是转发个人的，还是频道的，还是私人频道的，得到这个信息
     * @param  {[type]} message [description]
     * @return {[type]}         [description]
     */
    value: function getMessageFwdFromInfo(message) {
      var resp = {};
      // 投稿者转发频道
      var fwdChannel = message.forward_from_chat;
      var fwdUser = message.forward_from;
      var user = message.from;

      if (fwdChannel) {
        var channel_username = fwdChannel.username;
        if (!channel_username) {
          resp = this.getUser(user);
          resp.type = 'channel_private';
          resp.channel = _core.helper.formatText(fwdChannel.title);
        } else {
          resp = this.getUser(user);
          resp.type = 'channel';
          resp.channel_username = channel_username;
          resp.channel = _core.helper.formatText(fwdChannel.title);
          resp.msgid = message.forward_from_message_id;
        }
      } else if (fwdUser) {
        // 投稿者转发自别人
        var from = this.getUser(fwdUser);
        resp = this.getUser(user);
        resp.type = 'forward_user';
        resp.from = from.username;
        resp.fromuserid = from.userid;
      } else {
        resp = this.getUser(user);
        resp.type = 'user';
      }
      return resp;
    }
  }, {
    key: "getFromText",

    /**
     * 获取来源(如果是转发别人的信息): @xxx 一行
     * @param  {[type]} message [description]
     * @return {[type]}         [description]
     */
    value: function getFromText(message) {
      var fwdInfo = this.getMessageFwdFromInfo(message);
      var text = '';
      if (fwdInfo.type == 'channel_private') {
        text = this.get('sub_from_channel_private', fwdInfo);
      } else if (fwdInfo.type == 'channel') {
        text = this.get('sub_from_channel', fwdInfo);
      } else if (fwdInfo.type == 'forward_user') {
        text = this.get('sub_from', fwdInfo);
      }
      return text;
    }
  }, {
    key: "getFromReserve",

    /**
     * 得到 来源保留：保留/匿名 这一行
     * @param  {[type]} type [description]
     * @return {[type]}      [description]
     */
    value: function getFromReserve(type) {
      var text = this.get('from_real');
      if (type == this.vars.SUB_ANY) {
        text = this.get('from_anonymous');
      }
      text = this.get('sub_from_reserve', { reserve: text });
      return text;
    }
  }, {
    key: "getSubUser",

    /**
     * 获取投稿人:@xxx 一行
     * @param  {[type]} message [description]
     * @return {[type]}         [description]
     */
    value: function getSubUser(message) {
      return this.get('sub_people', this.getUser(message.from));
    }
  }, {
    key: "getMoreHelp",

    /**
     * 获取一行： 更多帮助 [/command] 的文本
     * @return {[type]} [description]
     */
    value: function getMoreHelp() {
      return this.get('admin_morehelp', { command: '/pwshelp' });
    }
  }, {
    key: "getAdminCommonHeader",

    /**
     * 获取审稿群通用头部
     * @param  {[type]} message [description]
     * @return {[type]}         [description]
     */
    value: function getAdminCommonHeader(message) {
      var text = this.get('sub_new') + "\n" + this.getSubUser(message);
      // 是投稿人转发的信息，获取消息之来源
      if (message.forward_date) {
        text += "\n" + this.getFromText(message);
      }
      text += "\n" + this.getFromReserve(message.sub_type);
      return text;
    }
  }, {
    key: "getAdminAction",

    /**
     * 机器人将稿件转发至审稿群后，询问管理员如何操作的文案
     * 如 新投稿\n投稿人:xx\n...
     * @param  {String} type    操作类型
     * @param  {Object} message 稿件
     * @return {[type]}         [description]
     */
    value: function getAdminAction(message) {
      var text = this.getAdminCommonHeader(message);
      text += "\n\n" + this.getMoreHelp();
      return text;
    }
  }, {
    key: "language",

    /**
     * 获取语言包JSON
     * @return {[type]} [description]
     */
    get: function get() {
      if (!this.lang) {
        this.lang = require("../lang/" + this.langName + ".json");
      }
      return this.lang;
    }
  }]);
  return Lang;
}();

exports.default = Lang;